USE [master]
GO
/****** Object:  Database [ClothingStore]    Script Date: 31/08/2017 10:30:42 ******/
CREATE DATABASE [ClothingStore]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ClothingStore', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ClothingStore.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ClothingStore_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ClothingStore_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ClothingStore] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ClothingStore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ClothingStore] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ClothingStore] SET ARITHABORT OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ClothingStore] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ClothingStore] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ClothingStore] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ClothingStore] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ClothingStore] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ClothingStore] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ClothingStore] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ClothingStore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ClothingStore] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ClothingStore] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ClothingStore] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ClothingStore] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ClothingStore] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ClothingStore] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ClothingStore] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ClothingStore] SET  MULTI_USER 
GO
ALTER DATABASE [ClothingStore] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ClothingStore] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ClothingStore] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ClothingStore] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ClothingStore] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ClothingStore]
GO
/****** Object:  Table [dbo].[Admins]    Script Date: 31/08/2017 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admins](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admins] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 31/08/2017 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clothes]    Script Date: 31/08/2017 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clothes](
	[ClothId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [money] NULL,
	[Image] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Clothes] PRIMARY KEY CLUSTERED 
(
	[ClothId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Companies]    Script Date: 31/08/2017 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Items]    Script Date: 31/08/2017 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admins] ON 

INSERT [dbo].[Admins] ([AdminID], [Password], [AdminName]) VALUES (1, N'8923', N'מרים')
INSERT [dbo].[Admins] ([AdminID], [Password], [AdminName]) VALUES (2, N'4572', N'ברכה')
SET IDENTITY_INSERT [dbo].[Admins] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (1, N'נשים')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (2, N'גברים')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (3, N'ילדים')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (4, N'תינוקות')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Clothes] ON 

INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (2, 2, 2, 4, 66.0000, 6.0000, N'82424536-f5c0-48ae-a452-7863f1edd19b.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (16, 2, 1, 3, 40.0000, NULL, N'6d215b67-d098-4b69-a4a2-3007e1552655.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (17, 1, 1, 3, 200.0000, NULL, N'0482d3c1-df57-4f2d-9a85-1e4f4bde0c48.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (18, 1, 1, 1, 200.0000, NULL, N'717f8173-6f30-408d-98d8-49a592559b50.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (21, 3, 1, 3, 400.0000, NULL, N'7b320441-6200-4f71-bc0b-863a5f5e438a.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (22, 1, 1, 2, 300.0000, NULL, N'df1477d0-79c1-4643-adaf-1162ef853c66.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (24, 1, 1, 1, 55.0000, NULL, N'46b2f185-1c08-4896-be19-b2bd88876e37.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (25, 2, 2, 4, 344.0000, NULL, N'de35a303-800d-4531-bb44-92d4aaf209ab.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (26, 1, 2, 1, 200.0000, NULL, N'30ddadec-a7a8-42f5-a018-fb303248a3e1.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (27, 2, 1, 7, 222.0000, NULL, N'5b375ae1-dd5a-4ac6-9ac7-c7193dcb2fef.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (29, 1, 1, 3, 111.0000, NULL, N'3092a779-bb7b-4eb8-9120-f69e46d61bf8.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (30, 1, 1, 1, 300.0000, NULL, N'ec34c301-597f-420e-87fe-31e70ada61a5.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (31, 2, 1, 3, 222.0000, NULL, N'ede2e95e-a613-4a00-91c4-1b154a9793b2.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (32, 1, 1, 1, 500.0000, NULL, N'dffbd0b9-2fcd-44ea-84b6-cd5a81bdbe58.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (35, 2, 1, 1, 222.0000, NULL, N'1ab46c8f-022a-48b7-a99e-cce6369bcd27.JPG')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (37, 1, 1, 1, 222.0000, NULL, N'321a51c7-7c0c-40d3-bbf6-a8aff6a48c61.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (42, 2, 2, 8, 200.0000, NULL, N'5212b96a-422f-4512-9dbc-a2543bafc4e3.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (43, 2, 1, 9, 80.0000, NULL, N'9b2192c1-64be-4de3-a878-c66f9572e124.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (46, 3, 2, 3, 40.0000, NULL, N'ce58b4f9-1801-4d1d-913a-d847ec196fbe.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (47, 2, 1, 3, 100.0000, NULL, N'8f2aa1da-06ff-40ac-af9d-8bfd32c56866.png')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (48, 4, 2, 1, 100.0000, NULL, N'b49aa2bd-4669-4088-ab7a-a6f7f8560eb9.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (49, 4, 2, 6, 60.0000, NULL, N'1f9f1a1f-23d3-41ca-9ef7-d9a484508bd4.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (56, 4, 1, 1, 78.0000, NULL, N'3b306244-2446-43ae-a5ee-ad69eebf51ec.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (57, 4, 1, 7, 80.0000, NULL, N'9337ed09-b7f5-40e1-9f54-5c77d523bbfd.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (58, 4, 1, 10, 90.0000, NULL, N'053fb3be-cf8c-4c9c-a63a-ffdb6a83f39b.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (59, 2, 1, 6, 99.0000, NULL, N'9dd1b9ee-da66-49ad-944e-55b54a113bbb.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (60, 3, 2, 9, 99.0000, NULL, N'97512bb2-a76f-4ee7-a04f-b325fff43e7f.jpg')
INSERT [dbo].[Clothes] ([ClothId], [CategoryId], [CompanyId], [ItemId], [Price], [Discount], [Image]) VALUES (61, 3, 1, 6, 70.0000, NULL, N'78cdadd3-6dac-43a3-a1bd-5cf9eea85a5e.jpg')
SET IDENTITY_INSERT [dbo].[Clothes] OFF
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([CompanyId], [CompanyName]) VALUES (1, N'קאסטרו')
INSERT [dbo].[Companies] ([CompanyId], [CompanyName]) VALUES (2, N'זארה')
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[Items] ON 

INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (1, N'שמלה')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (2, N'חצאית')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (3, N'חולצה')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (4, N'חליפה')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (5, N'עניבה')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (6, N'סריג')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (7, N'מעיל')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (8, N'מכנסיים')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (9, N'צעיף')
INSERT [dbo].[Items] ([ItemId], [ItemName]) VALUES (10, N'כובע')
SET IDENTITY_INSERT [dbo].[Items] OFF
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Categories]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([CompanyId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Companies]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Items] ([ItemId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Items]
GO
USE [master]
GO
ALTER DATABASE [ClothingStore] SET  READ_WRITE 
GO
