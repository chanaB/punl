﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seldat {
    public class Admin {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int AdminId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string AdminName { get; set; }
    }
}
