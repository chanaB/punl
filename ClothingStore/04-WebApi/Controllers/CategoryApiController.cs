﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class CategoryApiController : ApiController {

        CategoryLogic logic = new CategoryLogic();

        [HttpGet]
        [Route("api/category")]
        public HttpResponseMessage GetAllCategories() {
            try {
                List<CategoryModel> categories = logic.GetAllCategory();
                return Request.CreateResponse(HttpStatusCode.OK, categories);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
