﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seldat {
    public class Item {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ItemId { get; set; }
        [Required]
        public string ItemName { get; set; }
        public virtual ICollection<Cloth> Clothes { get; set; }
        public virtual ICollection<Category> Categories { get; set; }

    }
}
