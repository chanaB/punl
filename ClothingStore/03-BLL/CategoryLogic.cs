﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class CategoryLogic : BaseLogic {

        public List<CategoryModel> GetAllCategory() {
            return db.Categories.Select(c => new CategoryModel { id = c.CategoryId, name = c.CategoryName }).ToList();
        }

    }
}
