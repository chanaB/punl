﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class ClothLogic : BaseLogic {

        public bool Login(AdminModel model) {
            return db.Admins.Any(m => m.AdminName == model.name && m.Password == model.password);
        }

        public List<ClothModel> GetCategoryClothes(int categoryid) {
            return db.Clothes.Include("Categories").Include("Items").Include("Companies")
                .Where(c => c.CategoryId == categoryid).Select(c => new ClothModel {
                id = c.ClothId,
                category = new CategoryModel { id = c.Category.CategoryId, name = c.Category.CategoryName },
                company = new CompanyModel { id = c.Company.CompanyId, name = c.Company.CompanyName },
                item = new ItemModel { id = c.Item.ItemId, name = c.Item.ItemName },
                image = c.Image,
                discount = c.Discount,
                price = c.Price
            }).ToList();
        }

        public List<ClothModel> GetCategoryClothesSale(int categoryid) {
            System.Console.WriteLine("clothןמעLogic");
            return db.Clothes.Include("Categories").Include("Items").Include("Companies")
                .Where(c => c.CategoryId == categoryid&&c.Discount>0).Select(c => new ClothModel {
                    id = c.ClothId,
                    category = new CategoryModel { id = c.Category.CategoryId, name = c.Category.CategoryName },
                    company = new CompanyModel { id = c.Company.CompanyId, name = c.Company.CompanyName },
                    item = new ItemModel { id = c.Item.ItemId, name = c.Item.ItemName },
                    image = c.Image,
                    discount = c.Discount,
                    price = c.Price
                }).ToList();
        }


        public ClothModel GetOneCloth(int id) {

            return db.Clothes.Include("Categories").Include("Items").Include("Companies")
                .Where(c => c.ClothId == id)
                .Select(c => new ClothModel {
                    id = c.ClothId,
                    category = new CategoryModel { id = c.Category.CategoryId, name = c.Category.CategoryName },
                    company = new CompanyModel { id = c.Company.CompanyId, name = c.Company.CompanyName },
                    item = new ItemModel { id = c.Item.ItemId, name = c.Item.ItemName },
                    image = c.Image,
                    discount = c.Discount,
                    price = c.Price
                }).FirstOrDefault();
        }

        public ClothModel UpdateFullCloth(ClothModel model) {
            Cloth UpdateCloth = db.Clothes.FirstOrDefault(c => c.ClothId == model.id);
            UpdateCloth.CompanyId = model.company.id;
            UpdateCloth.CategoryId = model.category.id;
            UpdateCloth.ItemId = model.item.id;
            UpdateCloth.Price = model.price;
            UpdateCloth.Discount = model.discount;
            UpdateCloth.Image = model.image;
            db.SaveChanges();
            return model;
        }

        public void updateImage(int id, string newFileName) {
            Cloth cloth = db.Clothes.FirstOrDefault(c => c.ClothId == id);
            cloth.Image = newFileName;
            db.SaveChanges();
        }

        public ClothModel AddCloth(ClothModel model) {
            Cloth cloth = new Cloth {
                CategoryId = model.category.id,
                CompanyId = model.company.id,
                ItemId = model.item.id,
                Price = model.price,
                Discount = model.discount,
                Image = model.image
            };
            db.Clothes.Add(cloth);
            db.SaveChanges();
            model.id = cloth.ClothId;
            return model;
        }

        public void RemoveCloth(int id) {
            Cloth removecloth = new Cloth { ClothId = id };
            db.Clothes.Attach(removecloth);
            db.Clothes.Remove(removecloth);
            db.SaveChanges();
        }


    }
}

