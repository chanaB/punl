﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class AdminApiController : ApiController {

        AdminLogic logic = new AdminLogic();

        [HttpPost]
        [Route("api/admin")]
        public HttpResponseMessage IsAdminExsist(AdminModel model) {
            try {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllError());
                return Request.CreateResponse(HttpStatusCode.OK, logic.GetAdminExsist(model));
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
