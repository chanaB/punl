﻿function add() {
    window.location.href = "addPage.html";
}

function showAllCategoriesForUpdate() {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/category",
        cache: false,
        error: function (err) 
            {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            for (i = 0; i < response.length; i++) {
                $("#containbuttons").append("<input type='button' class='allOption' value='" + response[i].name + "' onclick='showCategoryClothesForUpdate(" + response[i].id + ")'></button>");
            }
        }
    });
}

function logOut() {
    sessionStorage.removeItem("user");
    window.location.href = "index.html";
}

function showDetails(clothId) {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/clothes/" + clothId,
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            var discount = response.discount;
            if (discount == null)
                discount = 0;
            $("#myPopup").empty();
            $("#myPopup").append("<div id='updateForm'></div><hr /><p>" + response.item.name + " " + response.category.name + " " + response.company.name + " </br>מחיר:" + response.price + " </br>אחוזי הנחה:" + discount + "</p></br><input type='button' class='buttonUpdate'  data-pic='" + response.image + "' data-id='" + response.id + "' value='עדכון' onclick='fillUpdateForm(this)'/><input type='button' value='מחיקה' class='buttonUpdate' onclick='deleteCloth(" + response.id + ")'/> ");
        }
    });
}

function showCategoryClothesForUpdate(categoryId) {
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/CategoryClothes/" + categoryId,
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            $("#containbody").empty();
            for (var i = 0; i < response.length; i++) {
                $("#containbody").append("<a href='#myPopup' data-position-to='window' data-transition='slide' data-rel='popup' class='ui-btn ui-btn-inline ui-corner-all' onclick='showDetails(" + response[i].id + ")'><img src='http://localhost:58792/Images/" + response[i].image + "' /></a><div>");
            }
        }
    });
}

function deleteCloth(id) {
    $.ajax({
        method: "DELETE",
        url: "http://localhost:58792/api/clothes/" + id,
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            var x = document.createElement("DIALOG");
            var t = document.createTextNode("המחיקה עברה בהצלחה!!");
            x.setAttribute("open", "open");
            x.appendChild(t);
           document.body.appendChild(x);
            setTimeout(function () { document.body.removeChild(x); refresh(); }, 3000);
        }
    });
}

function refresh() {
    location.reload();
}

function addCloth() {
    var image = "";
    var files = $('#image').get(0).files;
    if (files.length > 0)
        image = "ksrjlk"
    else image = null;
    $.ajax({
        method: "POST",
        url: "http://localhost:58792/api/clothes",
        data: {
            category: {
                id: $("#categories").val(),
                name: $("#categories").find(':selected').text()
            },
            company: {
                id: $("#companies").val(),
                name: $("#companies").find(':selected').text()
            },
            item: {
                id: $("#items").val(),
               name: $("#items").find(':selected').text()
            },
            price: $("#price").val(),
            discount: $("#discount").val(),
            image: image
        },
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            var formData = new FormData();
            var files = $('#image').get(0).files;//get(0)זו פונקציה שמחזירה את האוביקט המקורי של ג'אוה סקריפט
            if (files.length > 0)
                formData.append("image", files[0]);
            formData.append("id", response.id);
            $.ajax({
                method: "POST",
                url: "http://localhost:58792/api/upload",
                data: formData,
                error: function (err) {
                    var errors = JSON.parse(err.responseText);
                    var error = "";
                    for (var i in errors)
                        error += errors[i] + " ";
                    alert(err.status + ", " + err.statusText + "----" + error);
                },
                success: function (response) {
                    var x = document.createElement("DIALOG");
                    var t = document.createTextNode("ההוספה עברה בהצלחה");
                    x.setAttribute("open", "open");
                    x.appendChild(t);
                    document.body.appendChild(x);
                    setTimeout(function () { document.body.removeChild(x); refresh(); }, 3000);
                },
                contentType: false,
                processData: false
            });
        }
    });
}

function fillUpdateForm(obj) {
    var image = obj.getAttribute('data-pic');
    var id = obj.getAttribute('data-id');
    $("#updateForm").empty();
    $("#updateForm").append("<p>בחר קטגוריה</p><select id='categories'>"
        + "</select ><p>בחר חברה</p> <select id='companies'></select> <p>בחר פריט</p> <select id='items'>"
        + "</select > <p>הכנס מחיר</p> <input type='number' id='price' /> <p>הכנס הנחה</p> <input type='number' id='discount' /><br/><input type='button' value='עדכן' data-id='" + id + "' data-pic='" + image + "' onclick='updateCloth(this)'/>");
    fillOptions();
}

function updateCloth(obj) {
    var image = obj.getAttribute('data-pic');
    var id = obj.getAttribute('data-id');
    $.ajax({
        method: "PUT",
        url: "http://localhost:58792/api/clothes/" + id,
        data: {
            category: {
                id: $("#categories").val(),
                name: $("#categories").find(':selected').text()
            },
            company: {
                id: $("#companies").val(),
                name: $("#companies").find(':selected').text()
            },
            item: {
                id: $("#items").val(),
                name: $("#items").find(':selected').text()
            },
            price: $("#price").val(),
            discount: $("#discount").val(),
            image: image
        },
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            var x = document.createElement("DIALOG");
            var t = document.createTextNode("העדכון עבר בהצלחה!!!");
            x.setAttribute("open", "open");
            x.appendChild(t);
            document.body.appendChild(x);
            setTimeout(function () { document.body.removeChild(x); refresh(); }, 3000);
        }
    });
}

function back() {
    window.location.href = "admin.html";
}

function visiblePage() {
    if (sessionStorage.getItem("user")) {
        $('#allPage').css("visibility", "visible");
        var name = sessionStorage.getItem("user");
        $("#welcome").empty();
        $("#welcome").append("<h1>" + name + "</h1>");
    }
    else {
        var x = document.createElement("DIALOG");
        var t = document.createTextNode("אין הרשאת כניסה!!!");
        x.setAttribute("open", "open");
        x.appendChild(t);
        document.body.appendChild(x);
        setTimeout(function () { document.body.removeChild(x);window.location.href="index.html" }, 3000);
    }
}

function fillOptions() {
    visiblePage();
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/category",
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            $("#categories").append("<option value=' '</option>");
            for (i = 0; i < response.length; i++) {
                $("#categories").append("<option value='" + response[i].id + "'>" + response[i].name + "</option>");
            }
        }
    });
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/company",
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            $("#companies").append("<option value=' '</option>");
            for (i = 0; i < response.length; i++) {
                $("#companies").append("<option value='" + response[i].id + "'>" + response[i].name + "</option>");
            }
        }
    });
    $.ajax({
        method: "GET",
        url: "http://localhost:58792/api/item",
        cache: false,
        error: function (err) {
            var errors = JSON.parse(err.responseText);
            var error = "";
            for (var i in errors)
                error += errors[i] + " ";
            alert(err.status + ", " + err.statusText + "----" + error);
        },
        success: function (response) {
            $("#items").append("<option value=' '</option>");
            for (i = 0; i < response.length; i++) {
                $("#items").append("<option value='" + response[i].id + "'>" + response[i].name + "</option>");
            }
        }
    });
}
