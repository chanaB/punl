﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seldat {
    public class Company {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int CompanyId { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public virtual ICollection<Cloth> Clothes { get; set; }
    }
}
